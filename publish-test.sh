#!/bin/bash
DOCKER_USER=$1
DOCKER_PASS=$2
IMAGE_NAME=bbb_`date '+%s'`
echo "Docker login"
docker login -u $DOCKER_USER -p $DOCKER_PASS
echo "Docker commit"
docker commit bbb_docker_build $IMAGE_NAME
echo "Docker tag"
docker tag $IMAGE_NAME andiksetyawan/bbb-dev:2.4.rc2
echo "Docker push"
docker push andiksetyawan/bbb-dev
echo "Docker logout"
docker logout
